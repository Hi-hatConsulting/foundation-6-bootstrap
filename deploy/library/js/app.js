/*jslint browser: true */
/*global $, jQuery, alert, console, Foundation6Bootstrap:true */


var console = console || { log: function() { 'use strict'; } };

window.Foundation6Bootstrap = window.Foundation6Bootstrap || {};

(function (s) {

    'use strict';

    s.App = function () {

        return {
            initialized: false,
            elements: {},
            settings: {
                debug: false,
                host_url: 'http://' + window.location.hostname + '/',
                development_url: 'http://foundation-6-bootstrap.dev/',
                staging_url: '',
                production_url: '',
                environment: ''
            },

            init: function () {

                if (this.settings.debug) { console.log('init()'); }

                if (this.initalized) { return false; }
                this.initialized = true;

                // dom elements
                this.elements.body =  $('body', 'html');
                this.elements.mainWrapper = $('.main-wrapper', this.elements.body);
                this.elements.debug = $('#txtDebug', this.elements.body);
                this.elements.currentYear = $('.current-year', this.elements.body);

                // set the environment setting
                switch (this.settings.host_url){
                    case this.settings.development_url:
                        this.settings.environment = 'development';
                        break;
                    case this.settings.staging_url:
                        this.settings.environment = 'staging';
                        break;
                    case this.settings.production_url:
                        this.settings.environment = 'production';
                        break;
                    default:
                        this.settings.environment = 'production';
                }
                if(this.settings.debug){ console.log('Environment: ' + this.settings.environment); };

                // configure debug based on config file
                if (this.elements.debug.val() === 'true') {
                    this.settings.debug = true;
                    this.initDebug();
                }

                // initialize foundation
                $(document).foundation();

                // populate current year for copyrite in footer
                var d = new Date();
                this.elements.currentYear.html(d.getFullYear());

            },


            initDebug: function () {

                if (this.settings.debug) { console.log('initDebug()'); }

                $(this.elements.body).append('<div id="debug-message"></div>');
                $('#debug-message').append('<p class="small">small</p><p class="medium">medium</p><p class="large">large</p><p class="exlarge">extra large</p>');
                $(window).resize(function () {
                    $('#debug-message').empty();
                    $('#debug-message').append('<p class="small">small</p><p class="medium">medium</p><p class="large">large</p><p class="exlarge">extra large</p>');
                    $('#debug-message').append('<p>width: ' + window.innerWidth + '</p>');
                });

            }

        };

    };

}(Foundation6Bootstrap));


$(document).ready(function() {
    'use strict';
    Foundation6Bootstrap.App().init();
});




